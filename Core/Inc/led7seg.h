#ifndef __LED_7_SEG_H
#define __LED_7_SEG_H

#ifdef __cplusplus
extern "C" {
#endif
#include "stm32f1xx_hal.h"
#define a_Pin GPIO_PIN_0
#define a_GPIO_Port GPIOA
#define b_Pin GPIO_PIN_1
#define b_GPIO_Port GPIOA
#define c_Pin GPIO_PIN_2
#define c_GPIO_Port GPIOA
#define d_Pin GPIO_PIN_3
#define d_GPIO_Port GPIOA
#define e_Pin GPIO_PIN_4
#define e_GPIO_Port GPIOA
#define f_Pin GPIO_PIN_5
#define f_GPIO_Port GPIOA
#define g_Pin GPIO_PIN_6
#define g_GPIO_Port GPIOA
#define led1_Pin GPIO_PIN_0
#define led1_GPIO_Port GPIOB
#define led2_Pin GPIO_PIN_1
#define led2_GPIO_Port GPIOB
#define led3_Pin GPIO_PIN_3
#define led3_GPIO_Port GPIOB
#define led4_Pin GPIO_PIN_4
#define led4_GPIO_Port GPIOB
void print_led_7_seg(int16_t num);

#ifdef __cplusplus
}
#endif

#endif /* LED_7_SEG */
