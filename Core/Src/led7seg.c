#include "led7seg.h"

void fail()
{
HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_RESET);
}
void number_0()
{
HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_SET);
}
void number_1()
{
HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_SET);
}
void number_2()
{
HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_RESET);
}
void number_3()
{
HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_RESET);
}
void number_4()
{
HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_RESET);
}
void number_5()
{
HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_RESET);
}
void number_6()
{
HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_RESET);
}
void number_7()
{
HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_SET);
}
void number_8()
{
HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_RESET);
}
void number_9()
{
	HAL_GPIO_WritePin(GPIOA,a_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,b_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,c_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,d_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,e_Pin,GPIO_PIN_SET);
HAL_GPIO_WritePin(GPIOA,f_Pin,GPIO_PIN_RESET);
HAL_GPIO_WritePin(GPIOA,g_Pin,GPIO_PIN_RESET);
}
void print_one_led_7_seg(uint8_t num)
{
switch (num)
{
    case 0:
      number_0();
      break;
    case 1:
      number_1();
      break;
		case 2:
      number_2();
      break;
		 case 3:
      number_3();
      break;
		  case 4:
      number_4();
      break;
			 case 5:
      number_5();
      break;
			  case 6:
      number_6();
      break;
				 case 7:
      number_7();
      break;
				  case 8:
      number_8();
      break;
					 case 9:
     number_9();
      break;
}
}
void print_led_7_seg(int16_t num)
{
	uint16_t data[4];
	data[0]=num;
	data[1]=num;
	data[2]=num;
	data[3]=num;
	data[0]=data[0]%10;
	data[1]=data[1]%100;
	data[1]=data[1]/10;
	data[2]=data[2]%1000;
	data[2]=data[2]/100;
	data[3]=num/1000;
for(int i=0;i<4;i++)
	{
	switch (i)
	{
    case 0:
      // statements
			HAL_GPIO_WritePin(led1_GPIO_Port,led1_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(led2_GPIO_Port,led2_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(led3_GPIO_Port,led3_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(led4_GPIO_Port,led4_Pin,GPIO_PIN_RESET);
			if(num>9999||num<0)
			{
			fail();
			}else
			{
			print_one_led_7_seg(data[0]);				
			}
			HAL_Delay(2);
      break;

    case 1:
			HAL_GPIO_WritePin(led1_GPIO_Port,led1_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(led2_GPIO_Port,led2_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(led3_GPIO_Port,led3_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(led4_GPIO_Port,led4_Pin,GPIO_PIN_RESET);
		if(num>9999||num<0)
			{
			fail();
			}else
			{
			print_one_led_7_seg(data[1]);
			}
						HAL_Delay(2);
      // statements
      break;
    case 2:
      // statements
			HAL_GPIO_WritePin(led1_GPIO_Port,led1_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(led3_GPIO_Port,led2_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(led4_GPIO_Port,led3_Pin,GPIO_PIN_SET);
			HAL_GPIO_WritePin(led4_GPIO_Port,led4_Pin,GPIO_PIN_RESET);
				if(num>9999||num<0)
			{
			fail();
			}else
			{
			print_one_led_7_seg(data[2]);
			}
						HAL_Delay(2);
      break;
		    case 3:
			HAL_GPIO_WritePin(led1_GPIO_Port,led1_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(led2_GPIO_Port,led2_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(led3_GPIO_Port,led3_Pin,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(led4_GPIO_Port,led4_Pin,GPIO_PIN_SET);
						if(num>9999||num<0)
			{
			fail();
			}else
			{
			print_one_led_7_seg(data[3]);
			}
						HAL_Delay(2);
      // statements
      break;
      // default statements
}
	}
}
